#ifndef SETTIME_H
#define SETTIME_H

#include <Arduino.h>
#include <Time.h>
#include <DS1307RTC.h>

// #define DEBUG
#ifdef DEBUG
#include <Wire.h>
#endif

void setTime(void);
bool getTime(const char *str);
bool getDate(const char *str);

#endif
